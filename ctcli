#!/usr/bin/env python3
"""
    CLI for the chroot-task library
    Wolfgang Frisch <wfr@roembden.net>
"""

import argparse
import os
import logging
import sys
import pprint

import jails

if __name__ == "__main__":
    THIS_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
    DEFAULT_VAR_DIR = os.path.join(THIS_DIRECTORY, "var")

    # Command-line
    parser = argparse.ArgumentParser(description='run tasks inside chroots')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--directory',
                        default=DEFAULT_VAR_DIR,
                        help="variable data (default: %(default)s")
    subparsers = parser.add_subparsers(help='subcommand help',
                                       title="subcommands")

    parser_add = subparsers.add_parser('add', help="add new task")
    parser_add.add_argument('--url',
                            help="file system tarball", required=True)
    parser_add.add_argument('--command-line',
                            help="command line",
                            required=True)
    parser_add.set_defaults(command='add')

    parser_list = subparsers.add_parser('list',
                                        help="list tasks")
    parser_list.set_defaults(command='list')

    parser_alive = subparsers.add_parser('alive',
                                         help="is the task still running?")
    parser_alive.add_argument('--task', type=int,
                              help="Task ID",
                              required=True)
    parser_alive.set_defaults(command='alive')

    parser_signal = subparsers.add_parser('signal',
                                          help="send POSIX signal")
    parser_signal.add_argument('--task', type=int,
                               help="Task ID",
                               required=True)
    parser_signal.add_argument('--signal', type=str,
                               help="POSIX signal, e.g. SIGKILL",
                               required=True)
    parser_signal.set_defaults(command='signal')
    
    parser_output = subparsers.add_parser('output',
                                          help='print stdout and stderr')
    parser_output.add_argument('--task', type=int,
                               help="Task ID",
                               required=True)
    parser_output.set_defaults(command='output')

    parser_delete = subparsers.add_parser('clean',
                                          help="clean up old tasks")
    parser_delete.set_defaults(command='clean')

    args = parser.parse_args()
    if not hasattr(args, 'command'):
        parser.print_help()
        sys.exit(1)

    # Logging
    LOG_FORMAT = '[%(asctime)-15s %(levelname)6s %(name)s] %(message)s'
    if args.verbose:
        logging.basicConfig(format=LOG_FORMAT, level=logging.DEBUG)
    else:
        logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)

    # Main
    environment = jails.environment.Environment(args.directory)
    taskman = jails.task.TaskManager(environment)
    try:
        if args.command == 'list':
            tasks = taskman.get_tasks()
            for task_id in sorted(tasks.keys()):
                pprint.pprint(tasks[task_id])
        elif args.command == 'add':
            taskman.add(args.url, args.command_line)
        elif args.command == 'alive':
            print(taskman.alive(args.task))
        elif args.command == 'signal':
            taskman.signal(args.task, args.signal)
        elif args.command == 'output':
            stdout, stderr = taskman.get_output(args.task)
            print("=== stdout: ===\n{}".format(stdout))
            print("=== stderr: ===\n{}".format(stderr))
        elif args.command == 'clean':
            taskman.clean()
    except jails.image.ImageException as e:
        logging.error(e)
        sys.exit(2)
    except jails.chroot.ChrootException as e:
        logging.error(e)
        sys.exit(2)
    except jails.task.TaskException as e:
        logging.error(e)
        sys.exit(2)

# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:
