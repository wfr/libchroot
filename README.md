# Overview
*libchroot* is a chroot wrapper.

It downloads file system images from the Internet, unpacks them, and runs tasks within them.

## Environment
The library operates with directory environments. By default, the base directory is relative to the source code: `./var/`.

	var/images/               # file system image cache
	var/chroots/              # chroot environments
	var/state.sqlite3         # persistent state tracking

## Images
File system images are *tar* archives. All archives that can be unpacked by `tar -xaf` are supported.

The local name of an image is derived from its URL: `${base name of URL}-${SHA1 hash of URL}`.

For example the URL `https://wfrsw.de/tmp/images/alpine-rootfs.tar.xz` is stored as `alpine-rootfs.tar.xz-6f0d441b55354176d4d6a98e5fcf72bc573d8897`.

## Chroots
Chroots are simply unpacked images.

## Tasks
Each tasks runs within its own copy of a file system image. Tasks can be interacted with in various ways:

- Add a task, i.e. a file system URL and a *command-line*.
- List tasks
- Check if a task is alive
- Send POSIX signals to a task
- Get the stdout & stderr of a task
- Remove stale tasks

Tasks are identified by a *unique automatically generated ID*.


# Command-Line Interface
## Overview
	usage: ctcli [-h] [--verbose] [--directory DIRECTORY]
				 {add,list,alive,signal,output,clean} ...

	run tasks inside chroots

	optional arguments:
	  -h, --help            show this help message and exit
	  --verbose
	  --directory DIRECTORY
							variable data (default:
							/home/smb/src/giantswarm/libchroot.git/var

	subcommands:
	  {add,list,alive,signal,output,clean}
							subcommand help
		add                 add new task
		list                list tasks
		alive               is the task still running?
		signal              send POSIX signal
		output              print stdout and stderr
		clean               clean up old tasks


## Subcommand help
	$ ./ctcli add --help
	usage: ctcli add [-h] --url URL --command-line COMMAND_LINE

	optional arguments:
	  -h, --help            show this help message and exit
	  --url URL             file system tarball
	  --command-line COMMAND_LINE
							command line


## Example session
	$ ./ctcli add --url https://wfrsw.de/tmp/images/alpine-rootfs.tar.xz --command-line 'echo "Hello World"; sleep 30;'
	[2015-05-20 10:55:18,276   INFO root] Downloading image https://wfrsw.de/tmp/images/alpine-rootfs.tar.xz (alpine-rootfs.tar.xz-6f0d441b55354176d4d6a98e5fcf72bc573d8897)
	[2015-05-20 10:55:19,420   INFO root] Chroot unpacked: /home/smb/src/giantswarm/libchroot.git/var/chroots/1
	[2015-05-20 10:55:19,666   INFO root] Task new, ID = 1, PID = 987

	$ ./ctcli list
	{'chroot_path': '/home/smb/src/giantswarm/libchroot.git/var/chroots/1',
	 'command_line': 'echo "Hello World"; sleep 30;',
	 'datetime': '2015-05-20T10:55:41.756546',
	 'id': 1,
	 'pid': 1061,
	 'stderr': None,
	 'stdout': None,
	 'url': 'https://wfrsw.de/tmp/images/alpine-rootfs.tar.xz'}

	$ ./ctcli alive --task 1
	True

	$ ./ctcli signal --task 1 --signal SIGKILL
	[2015-05-20 10:56:53,852   INFO root] Task 2 - sent signal SIGKILL to PID 1218

	$ ./ctcli output --task 1
	=== stdout: ===
	b'Hello World\n'
	=== stderr: ===
	b''

	$ ./ctcli clean
	[2015-05-20 10:57:35,622   INFO root] Task removed: 1



# Notes

## Handled errors
- malformed URLs
- download errors
- invalid archives
- child exits abnormally 

Any other exceptions are not ignored, but not handled specifically.

## Caveats
- Processes are tracked by their PID only. This is not 100% reliable. A proper solution could use Linux cgroups, as it is done by `systemd` for example.
- Free disk space is not considered yet.

## Installation
The only dependency is a recent version of *Python 3*.

