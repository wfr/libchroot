#!/usr/bin/env python3
import os
import logging
import subprocess
import re
import shutil


class ChrootException(Exception):
    pass


class ChrootManager(object):
    """
        Manages chroot directories.
    """
    def __init__(self, environment):
        self.env = environment

    def unpack(self, image, chroot_name):
        """
            Unpack *image* to a directory *chroot_name*.
            Throws: ImageException, ChrootException
            
            FIXME:
            - Single Responsibility Principle! Doesn't belong here.
        """
        image.download()
        chroot_path = os.path.join(self.env.dirs['chroots'], chroot_name)
        if os.path.exists(chroot_path):
            raise ChrootException("Chroot already exists: "
                                  "{}".format(chroot_name))
        else:
            os.makedirs(chroot_path)
        logging.info("Chroot unpacked: {}".format(chroot_path))

        try:
            subprocess.check_call(['tar', '-C', chroot_path,
                                   '-xaf', image.path])
        except subprocess.CalledProcessError as e:
            raise ChrootException(e)
        return chroot_path

    def delete(self, chroot_path):
        """Delete the chroot directory, more or less safely.
        """
        if not os.path.isdir(chroot_path):
            raise ChrootException("chroot does not exist: " + chroot_path)
        if re.match(r'^/$', chroot_path) or \
           re.match(r'^/etc/?$', chroot_path) or \
           re.match(r'^/home/?$', chroot_path) or \
           re.match(r'^/home/[^/]+/?$', chroot_path):
            logging.warning("chroot not deleted (fail-safe): " + chroot_path)
        else:
            logging.debug("rmtree {}".format(chroot_path))
            try:
                shutil.rmtree(chroot_path)
            except Exception as e:
                raise ChrootException(e)


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:
