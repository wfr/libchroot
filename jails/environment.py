#!/usr/bin/env python3
import os
import logging
import sqlite3

class Environment(object):
    '''
        An environment instance.
        
        Contains images, chroots and the state database.
    '''
    def __init__(self, var_directory):
        self.dirs = {
            'var': var_directory,
            'images': os.path.join(var_directory, "images"),
            'chroots': os.path.join(var_directory, "chroots")
        }
        for directory in self.dirs.values():
            if not os.path.isdir(directory):
                logging.debug("mkdir {}".format(directory))
                os.makedirs(directory)  # throws PermissionError
        logging.debug("Environment: {}".format(var_directory))
        self.init_database()
        
    @property
    def var_directory(self):
        return self.dirs["var"]
    
    @property
    def images_directory(self):
        return self.dirs["images"]
    
    @property
    def chroots_directory(self):
        return self.dirs["chroots"]
    
    @property
    def db_path(self):
        return os.path.join(self.dirs["var"], "state.sqlite3")
    
    def init_database(self):
        """Create database tables if necessary.""" 
        db = sqlite3.connect(self.db_path)
        cur = db.cursor()
        # TODO: this table is not used yet
        cur.execute('''
            CREATE TABLE IF NOT EXISTS `images` (
                `url`    TEXT,
                `last_modified`    TEXT
            );''')
        cur.execute('''
            CREATE TABLE IF NOT EXISTS `tasks` (
                `id`           INTEGER PRIMARY KEY AUTOINCREMENT,
                `url`          TEXT NOT NULL,
                `command_line` TEXT,
                `chroot_path`  TEXT,
                `datetime`     TEXT,
                `pid`          INTEGER,
                `stdout`       TEXT,
                `stderr`       TEXT
        );''')
        
    def get_database(self):
        return sqlite3.connect(self.db_path)
        
        


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:
