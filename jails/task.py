#!/usr/bin/env python3
import os
import subprocess
import logging
import datetime
import signal

from .image import Image
from .chroot import ChrootManager

# class Task(object):
#    def __init__(self):
#        self.url = None
#        self.command_line = None
#        self.chroot_path = None
#        self.datetime = None
#        self.tid = None


class TaskException(Exception):
    pass


class TaskManager(object):
    """
        Manage a collection of tasks (within chroots).
        
        TODO:
        I did not want this exercise to have any external dependencies.
        A real-world program should use an object-relational mapper such as
        SQLAlchemy.
    """
    def __init__(self, environment):
        self.env = environment
        self.db = self.env.get_database()
        self.chrootman = ChrootManager(self.env)

    def get_tasks(self):
        """Return a dict of all tasks: {'id': { ... }}"""
        tasks = {}
        cur = self.db.cursor()
        cur.execute('''SELECT id, url, command_line, datetime, pid, '''
                    '''chroot_path, stdout, stderr '''
                    '''FROM tasks''')
        for row in cur.fetchall():
            task = {
                'id': row[0],
                'url': row[1],
                'command_line': row[2],
                'datetime': row[3],
                'pid': row[4],
                'chroot_path': row[5],
                'stdout': row[6],
                'stderr': row[7]
            }
            tasks[task['id']]= task
        return tasks

    def get_task(self, tid):
        cur = self.db.cursor()
        cur.execute('''SELECT id, url, command_line, datetime, pid, '''
                    '''chroot_path, stdout, stderr '''
                    '''FROM tasks WHERE id = ?''', (tid,))
        row = cur.fetchone()
        return {
                'id': row[0],
                'url': row[1],
                'command_line': row[2],
                'datetime': row[3],
                'pid': row[4],
                'chroot_path': row[5],
                'stdout': row[6],
                'stderr': row[7],
        }

    def exists(self, tid):
        cur = self.db.cursor()
        cur.execute('''SELECT COUNT(*) FROM tasks WHERE id = ?''', (tid,))
        return cur.fetchone()[0] != 0

    def add(self, url, command_line):
        """
            Add a new task, based on the image located at *url*.
            Runs *command_line* within the image.
        """
        cur = self.db.cursor()
        cur.execute('''INSERT INTO tasks (url, command_line) VALUES (?,?)''',
                    (url, command_line))
        self.db.commit()
        tid = cur.lastrowid
        
        image = Image(self.env, url)
        chroot_path = self.chrootman.unpack(image, str(tid))
        
        proc = subprocess.Popen(['chroot', chroot_path, '/bin/sh', '-c', 
                                 command_line], stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        logging.info("Task new, ID = {}, "
                     "PID = {}".format(tid, proc.pid))
        cur.execute('''UPDATE tasks SET datetime=?, pid=?, chroot_path=? '''
                    '''WHERE id=?''',
                    (datetime.datetime.now().isoformat(), proc.pid,
                     chroot_path, tid))
        self.db.commit()
        stdout, stderr = proc.communicate()
        cur.execute('''UPDATE tasks SET stdout=?, stderr=? WHERE id=?''',
                    (stdout, stderr, tid))
        self.db.commit()
        if proc.returncode != 0:
            raise TaskException("child exited abnormally with "
                                "$? == {}".format(proc.returncode))

    def alive(self, tid):
        """Return True if the task is still running."""
        if not self.exists(tid):
            raise TaskException('Task {} does not exist'.format(tid))
        try:
            os.kill(self.get_task(tid)['pid'], 0)
        except OSError:
            return False
        return True

    def signal(self, tid, signal_name):
        """Send a POSIX signal to the task.
        Args:
          tid (int): Task ID
          signal_name (str): POSIX signal (e.g. "SIGKILL")
        """
        signame = signal_name.upper()
        if signame.startswith("SIG") and hasattr(signal, signame):
            signum = getattr(signal, signame)
        else:
            raise TaskException("invalid signal: {}".format(signame))
        if self.alive(tid):
            task = self.get_task(tid)
            os.kill(task['pid'], signum)
            logging.info("Task {} - sent signal {} to PID {}"
                         "".format(tid, signame, task['pid']))
        else:
            logging.warn("Task not running. Not sending signal.")

    def get_output(self, tid):
        """Return (stdout, stderr)."""
        task = self.get_task(tid)
        return (task['stdout'], task['stderr'])

    def clean(self):
        """Clean up stale tasks."""
        for tid, task in self.get_tasks().items():
            if not self.alive(tid):
                self.chrootman.delete(task['chroot_path'])
                cur = self.db.cursor()
                cur.execute('''DELETE FROM tasks WHERE id=?''', (tid,))
                self.db.commit()
                logging.info("Task removed: {}".format(tid))


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:
