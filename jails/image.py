#!/usr/bin/env python3
import os
import urllib.parse
import urllib.request
import urllib.error
import hashlib
import logging

class ImageException(Exception):
    pass


class Image(object):
    """
        Represents a file system image.
    """
    def __init__(self, environment, url):
        self.env = environment
        self.url = url
        assert(os.path.exists(self.env.dirs['images']))

    @property
    def name(self):
        """Return a safe filename, based upon the URL."""
        return '{}-{}'.format(
            urllib.parse.quote(self.url.split('/')[-1]),
            hashlib.sha1(self.url.encode('UTF-8')).hexdigest())

    @property
    def path(self):
        """Return the full local path."""
        return os.path.join(self.env.dirs['images'], self.name)

    @property
    def mtime_local(self):
        """Return the local last modification time."""
        p = self.path + ".last-modified" 
        if os.path.exists(p):
            return open(p, "r").read()
        return None
    
    @mtime_local.setter
    def mtime_local(self, value):
        """Save the modification time locally."""
        with open(self.path + ".last-modified", "w") as lastmod:
            lastmod.write(value)
    
    @property
    def mtime_remote(self):
        """Return the Last-Modified HTTP header"""
        try:
            req = urllib.request.Request(self.url)
            req.get_method = lambda: 'HEAD'
            with urllib.request.urlopen(req) as response:
                return response.getheader('Last-Modified')
        except (ValueError,
                urllib.error.URLError,
                urllib.error.HTTPError) as e:
            raise ImageException(e)        

    def download(self):
        """
            Download the image file.
            can raise ImageException.
        """
        do_download = False
        mtime_local = self.mtime_local
        mtime_remote = self.mtime_remote
        if os.path.exists(self.path):
            logging.info("Image exists: {}".format(self.name))
            logging.debug("mtime_local:  {}".format(mtime_local))
            logging.debug("mtime_remote: {}".format(mtime_remote))
            if mtime_local != mtime_remote:
                logging.info("Remote image updated...")
                do_download = True
        else:
            do_download = True
        if do_download:
            logging.info("Downloading image " +
                         "{} ({})".format(self.url, self.name))
            # Throws exceptions:
            #   ValueError (Malformed URL)
            #   URLError   (invalid domain)
            #   HTTPError  (404 etc..)
            try:
                urllib.request.urlretrieve(self.url, filename=self.path)
                self.mtime_local = mtime_remote
            except (ValueError,
                    urllib.error.URLError,
                    urllib.error.HTTPError) as e:
                urllib.request.urlcleanup()
                raise ImageException(e)
        return self.path


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:
