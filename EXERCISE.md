# Giant Swarm application exercise
## Exercise
Write a library along with an example client in one of the
following languages: ruby, python, go, node.js, C/C++.

The purpose of the library is provide a chroot wrapper to execute images
downloadable from the Internet.

Via the library I want to start tasks. A task consists of the command to
execute and the URL to an image to download which contains the filesystem. I
want to be able to customize the process the task that gets started. Errors
should be properly forwarded, as the user of the library regularly seem to
provide malformed URLs / broken archives etc.

Besides starting the tasks, the library should provide me with the ability to
query the process health and sent signals.

An example client should be provided as well to provide this on the command
line.

### chroot
Chroot is a tool available on linux and mac to execute a process
with a different view on the filesystem.  So chroot myrootfs/ ls / shows you
the content of myrootfs/ because he ls process thinks that this is his root
filesystem.

### getting a test image
To get an image to play around with, you can actually
use docker: ``` docker run --name rootfs alpine ls docker export rootfs >
rootfs.tar ```

## Preliminary questions
> Q: The task should run *within* the chroot, not outside, correct? A: Yes
